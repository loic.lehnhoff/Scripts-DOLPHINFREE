# Whistles
This repository contains the files used to analyse whistle data obtained during the <a href="https://umr-marbec.fr/en/the-projects/dolphinfree/">DOLPHINFREE project</a>.    

## Description
Files '1-Identification_of_whistles.py' and '2-get_stats_of_whistles.py' were used during the DOLPHINFREE project.

File 'DECAV-solorun.py' is a simplified version that can be more easily used by anyone.

## Get Started
For classic use, only 'DECAV-solorun.py' and 'WhistleUtils.py' are needed. Copy them into a folder and run to see how to use the script.

## Results
Results are saved into .json files. Each file contains a dictionnary with :
- a set of key (integers) which are the ID of each whistle contour. 
- a set of coordinates ([[x coords],[y coords]]) for the pixels associated with each contour.