# Whistles
This repository contains the files used to analyse click data obtained during the <a href="https://umr-marbec.fr/en/the-projects/dolphinfree/">DOLPHINFREE project</a>.    

## Description
Files '1-detection_of_clicks.py', '2-get_stats_of_clicks.py', '3-projection_3features.py' and '3-projection_10features.py' were used during the DOLPHINFREE project.

File 'Detector-solorun.py' is a simplified version that can be more easily used by anyone.

## Get Started
For classic use, only 'Detector-solorun.py' and 'ClickUtils.py' are needed. Copy them into a folder and run to see how to use the script.

## Results
Results are saved into .csv files. Each file contains three columns :
- "time_(sec)" : the time at which a click is detected in the audio file (in seconds).
- "signal_amplitude" : the amplitude of the signal at that time.
- "TK_amplitude" : the amplitude of the Teager-Kaiser filtered signal at that time.